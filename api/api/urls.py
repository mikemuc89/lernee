# -*- coding: utf-8 -*-
from django.contrib import admin
from django.urls import path

from articles.urls import urlpatterns as articles_urls
from authorization.urls import urlpatterns as authorization_urls
from basis.urls import urlpatterns as basis_urls
from courses.urls import urlpatterns as courses_urls
from exams.urls import urlpatterns as exams_urls
from homepage.urls import urlpatterns as homepage_urls
from news.urls import urlpatterns as news_urls
from payments.urls import urlpatterns as payments_urls
from polls.urls import urlpatterns as polls_urls


urlpatterns = [
    path('admin/', admin.site.urls),
    *articles_urls,
    *authorization_urls,
    *basis_urls,
    *courses_urls,
    *exams_urls,
    *homepage_urls,
    *news_urls,
    *payments_urls,
    *polls_urls,
]
