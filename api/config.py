class Config(object):
    def __init__(self, conf):
        self.__dict__ = conf

CONFIG = Config({
    'MAIL': Config({
        'REPLY_TO': 'biuro@netgis.com.pl',
        'REGISTRATION_TEMPLATE': 'mails/registration.html',
        'REGISTRATION_TITLE': 'Rejestracja w serwisie Lernee',
        'RECOVER_TEMPLATE': 'mails/recover.html',
        'RECOVER_TITLE': 'Odzyskanie hasła w serwisie Lernee',
    }),
    'USERS': Config({
        'TOKEN_TIME_LIMIT': 1, # days
    })
})
