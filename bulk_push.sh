BRANCH=$1;
MESSAGE=$2;

REPO_ROOT=$(git rev-parse --show-toplevel);
CURRENT_DIR=$(pwd);

cd $REPO_ROOT;

echo "Bulk push tool";

echo $BRANCH;
echo $MESSAGE;

for i in $(git submodule status | awk '{ print $2; }'); do
  echo -- $i --
  cd $i;
  if [[ `git status --porcelain` ]]; then
    echo "Changes exist - pushing";
    git add -A;
    git commit -m "$2";
    git push origin $1;
  else
    echo "No changes";
  fi
  cd -;
done

cd $REPO_ROOT;

echo "-- Main repo check --"

if [[ `git status --porcelain` ]]; then
    echo "Changes exist - pushing";
    git add -A;
    git commit -m "$2";
    git push origin $1;
else
    echo "No changes";
fi

cd $CURRENT_DIR;

