const convict = require('convict');
const formats = require('convict-format-with-validator');

convict.addFormats(formats);

const config = convict({
  API_PORT: {
    format: 'port',
    default: 7002,
    env: 'API_PORT',
  },
  API_URL: {
    format: 'ipaddress',
    default: '127.0.0.1',
    env: 'API_URL',
  },
  MOCK_DATA: {
    format: 'Boolean',
    default: true,
    env: 'MOCK_DATA',
  },
  MOCK_MODE: {
    format: String,
    default: 'dev',
    env: 'MOCK_MODE',
  },
  NODE_ENV: {
    format: ['production', 'development', 'test'],
    default: 'development',
    env: 'NODE_ENV',
  },
  PORT: {
    format: 'port',
    default: 7001,
    env: 'API_PORT',
  },
});

config.validate({ allowed: 'strict' });

module.exports = config;
