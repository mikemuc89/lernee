import React from 'react';
import Enzyme, { shallow, render, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import renderer from 'react-test-renderer';

const { toMatchImageSnapshot } = require('jest-image-snapshot');

expect.extend({ toMatchImageSnapshot });

Enzyme.configure({ adapter: new Adapter() });

global.React = React;
global.renderer = renderer;

global.shallow = shallow;
global.render = render;
global.mount = mount;

global.TEST_THROW = '__TEST_THROW';
