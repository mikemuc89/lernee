#!/usr/bin/env bash

GITMODULES_PATH="./.gitmodules"
GITCONFIG_PATH="./.git/config"

PYIN="    "
JSIN="  "

if [ -z "$1" ]
  then
    echo "No submodule path supplied"
    exit 1
fi

if [ -z "$2" ]
  then
    echo "No repo name supplied"
    exit 1
fi

SUBMODULE_PATH=$1
REPO_NAME=$2

if [ -d "$SUBMODULE_PATH" ]
  then
    echo "Directory already exists"
    exit 1
fi

CONFIG_REPO_PATH=$(dirname "${SUBMODULE_PATH}")
APP_BASENAME=$(basename "${SUBMODULE_PATH}")
APP_BASENAME_UP="$(echo "$APP_BASENAME" | sed 's/.*/\u&/')"

GIT_SERVER="git@bitbucket.org"
GIT_USERNAME="mikemuc89"

GITMODULE_DEF_TEXT="[submodule \"$CONFIG_REPO_PATH/$REPO_NAME\"]\n\tpath = $SUBMODULE_PATH\n\turl = $GIT_SERVER:$GIT_USERNAME/$REPO_NAME.git"
GITCONFIG_DEF_TEXT="[submodule \"$CONFIG_REPO_PATH/$REPO_NAME\"]\n\turl = $GIT_SERVER:$GIT_USERNAME/$REPO_NAME.git\n\tactive = true"

echo -e "$GITMODULE_DEF_TEXT" >> $GITMODULES_PATH
echo -e "$GITCONFIG_DEF_TEXT" >> $GITCONFIG_PATH

git submodule update

declare -a DIRECTORIES=(
  "/"
  "/forms"
  "/forms/tests"
  "/models"
  "/models/tests"
  "/tests"
  "/utils"
  "/utils/tests"
  "/views"
  "/views/tests"
)

for dir in "${DIRECTORIES[@]}"
  do
    new_dir="$SUBMODULE_PATH$dir"
    if [ ! -d "$new_dir" ]
      then
        mkdir "$new_dir"
    fi
done

declare -A FILES=(
  ["/forms/__init__.py"]=""
  ["/forms/tests/__init__.py"]=""
  ["/models/__init__.py"]=""
  ["/models/tests/__init__.py"]=""
  ["/tests/__init__.py"]="from django.apps import apps\nfrom django.test import TestCase\n\nfrom $APP_BASENAME.apps import ${APP_BASENAME_UP}Config\nfrom $APP_BASENAME.forms.tests import *\nfrom $APP_BASENAME.models.tests import *\nfrom $APP_BASENAME.utils.tests import *\nfrom $APP_BASENAME.views.tests import *\n\n\nclass ConfigTest(TestCase):$PYINdef test_apps(self):\n$PYIN$PYINself.assertEqual($APP_BASENAME_UPConfig.name, '$APP_BASENAME')\n$PYIN$PYINself.assertEqual(apps.get_app_config('comments').name, 'comments')\n"
  ["/utils/__init__.py"]=""
  ["/utils/tests/__init__.py"]=""
  ["/views/__init__.py"]=""
  ["/views/tests/__init__.py"]=""
  ["/views/tests/mocks.json"]="{}"
  ["/__init__.py"]=""
  ["/.gitignore"]="*.pyc\n__pycache__/\nenv/\nmigrations/\n"
  ["/admin.py"]="from django.contrib import admin\n"
  ["/apps.py"]="from django.apps import AppConfig\n\n\nclass ${APP_BASENAME_UP}Config(AppConfig):\n$PYINname = '$APP_BASENAME'\n"
  ["/routes.py"]="from $APP_BASENAME import views\n\n\napp_routes = ()\n"
  ["/urls.py"]="from django.urls import path\nfrom $APP_BASENAME.routes import app_routes\n\n\nurlpatterns = [\n$PYINpath(url, resource.as_view(), name=resource.name) for (url, resource) in app_routes\n]\n"
)

for fl in "${!FILES[@]}"
  do
    new_file="$SUBMODULE_PATH$fl"
    if [ ! -f "$new_file" ]
      then
        touch "$new_file"
        echo -e "${FILES[$fl]}" >> "$new_file"
    fi
done


