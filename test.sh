TEST_MODULE=$1;
REPORT_PATH=$2;

REPO_ROOT=$(git rev-parse --show-toplevel);
CURRENT_DIR=$(pwd);

cd $REPO_ROOT;

echo "Test";
echo $TEST_MODULE;
echo $REPORT_PATH;

./env/bin/coverage erase;
if [[ $REPORT_PATH ]]; then
  if [[ -d $REPORT_PATH ]]; then
    ./env/bin/coverage run --branch --source="$REPORT_PATH" --omit="env/*,*/manage.py,*/wsgi.py,*/__init__.py,*/migrations/*,*/tests/test_*" ./api/manage.py test $TEST_MODULE;
    ./env/bin/coverage report -m;
  elif [[ -f $REPORT_PATH ]]; then
    ./env/bin/coverage run --branch --omit="env/*,*/manage.py,*/wsgi.py,*/__init__.py,*/migrations/*,*/tests/test_*" ./api/manage.py test $TEST_MODULE;
    ./env/bin/coverage report -m --include="$REPORT_PATH";
  fi
else
  ./env/bin/coverage run --branch --source="." --omit="env/*,*/manage.py,*/wsgi.py,*/__init__.py,*/migrations/*,*/tests/test_*" ./api/manage.py test $TEST_MODULE;
  ./env/bin/coverage report -m;
fi

cd $CURRENT_DIR;
